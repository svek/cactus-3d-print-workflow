#!/usr/bin/python

# what is working:
# 1) setup the geometry (ia. points)
# 2) in principle add point data. But not yet with
#     correct name but they appear in file and paraview.
#
# What is missing:
# 3) Associate an extend to the voxels or so. Currently,
#    it's not possible to display the points.
#
# -- 2017-03-21 18:20 Sven

# siehe auch: http://www.vtk.org/gitweb?p=VTK.git;a=blob;f=Examples/DataManipulation/Python/BuildUGrid.py
# huebsch auch: http://www.vtk.org/gitweb?p=VTK.git;a=blob;f=Examples/DataManipulation/Python/marching.py

import vtk
import sys, os
from vtk.util import numpy_support
import numpy as np
from numpy import array

#voxelPoints = vtk.vtkPoints()
#voxelTypes = vtk.vtkVoxel()
#for i in range(10):
#	pos = [ i, 0, 0 ]
#	# type, x, y, z
#	voxelPoints.InsertPoint(i, pos[0], pos[1], pos[2])
#	voxelTypes.GetPointIds().SetId(i, i)

grid = vtk.vtkUnstructuredGrid()
grid.Allocate(1,10)

gridpoints = \
array([[[ -1.  , -17.  ,  -1.  ],
        [ -0.5 , -17.  ,  -1.  ],
        [ -1.  , -16.5 ,  -1.  ],
        [ -0.5 , -16.5 ,  -1.  ],
        [ -1.  , -17.  ,  -0.5 ],
        [ -0.5 , -17.  ,  -0.5 ],
        [ -1.  , -16.5 ,  -0.5 ],
        [ -0.5 , -16.5 ,  -0.5 ]],

       [[ -1.  , -17.  ,  -0.75],
        [ -0.5 , -17.  ,  -0.75],
        [ -1.  , -16.5 ,  -0.75],
        [ -0.5 , -16.5 ,  -0.75],
        [ -1.  , -17.  ,  -0.25],
        [ -0.5 , -17.  ,  -0.25],
        [ -1.  , -16.5 ,  -0.25],
        [ -0.5 , -16.5 ,  -0.25]]])


# points for a single voxel or multiples...
voxelPoints = vtk.vtkPoints()
#voxelPoints.SetNumberOfPoints(8)


if False:
	voxelPoints.InsertPoint(0, 0, 0, 0)
	voxelPoints.InsertPoint(1, 1, 0, 0)
	voxelPoints.InsertPoint(2, 0, 1, 0)
	voxelPoints.InsertPoint(3, 1, 1, 0)
	voxelPoints.InsertPoint(4, 0, 0, 1)
	voxelPoints.InsertPoint(5, 1, 0, 1)
	voxelPoints.InsertPoint(6, 0, 1, 1)
	voxelPoints.InsertPoint(7, 1, 1, 1)
if True:
	for ig,p in enumerate(gridpoints):
		# one single voxel (!)
		aVoxel = vtk.vtkVoxel()
		print "Adding cell %d" % ig
		for ip,a in enumerate(p):
			print "Adding cell point %d, global point %d" % (ip, 8*ig+ip)
			voxelPoints.InsertPoint(8*ig+ip, a[0], a[1], a[2])
			aVoxel.GetPointIds().SetId(ip, 8*ig+ip)
		grid.InsertNextCell(aVoxel.GetCellType(), aVoxel.GetPointIds())


if False:
	aVoxel.GetPointIds().SetId(0, 0)
	aVoxel.GetPointIds().SetId(1, 1)
	aVoxel.GetPointIds().SetId(2, 2)
	aVoxel.GetPointIds().SetId(3, 3)
	aVoxel.GetPointIds().SetId(4, 4)
	aVoxel.GetPointIds().SetId(5, 5)
	aVoxel.GetPointIds().SetId(6, 6)
	aVoxel.GetPointIds().SetId(7, 7)


# setup the payload: vtkDoubleArray is a vtkDataArray
pointdata = numpy_support.numpy_to_vtk(np.arange(10), 0, vtk.VTK_DOUBLE)
pointdata = numpy_support.numpy_to_vtk(np.array([3.141]), 0, vtk.VTK_DOUBLE)
#voxelPoints.SetDataTypeToDouble()
#voxelPoints.SetData(pointdata)

grid.SetPoints(voxelPoints)

grid.GetCellData().SetActiveScalars("mypayload")
#grid.GetPointdata().SetFieldDataName("myfielddataname")
grid.GetCellData().SetScalars(pointdata)

print "Writing vtk out ..."

writer = vtk.vtkUnstructuredGridWriter()
writer.SetFileName("generated.vtk")
writer.SetFileTypeToASCII()
writer.SetInputData(grid)
writer.Write()

print "We are done"


# avoid free errors
os.system('kill %d' % os.getpid())
