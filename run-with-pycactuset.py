#!/usr/bin/python
#
## A Cactus Carpet HDF5 -> vtkUnstructuredGrid transformation program
## ==================================================================
##
## Implemented features:
##
##  * Manual ghost cell stripping
##  * Optional merging of individual reflevels before joining.
##  * Removing coarser cells which overlap finer cells
##  * Setup the cells and pointlists for the vtkUnstructuredGrid,
##    removing duplicate VTK point definitions in this step.
##  * Parallelism in the component dispatchment
##
## Missing:
##
##  * An automated way to detect badly aligned coarser and finer
##    blocks, leading to visible gaps. Currently, the workaround for
##    this is to make slices with Visit/Paraview accross all reflevels
##    (for instance in the coordinate center) both in the xy and xz
##    direction and ensuring that there are no gaps by fine tuning the
##    amount of cells removed at each reflevel in each direction.
##  * Symmetry restoration. Postcactus can only restore a reflection
##    symmetry and even this does not work for me.
##  * A CLI with parameter passing.
##
## Things which should also integrated into the workflow:
##
##  * Open the outputted VTK file with Visit and doing a contour plot,
##    exporting this to a STL database.
##  * Open the generated STL file with Blender, cut of smallish nonmanifold
##    sets and restore symmetries.
##
## Computational requirements
## ==========================
##
## For a large BNS run this script can run on a laptop with typical requirements
## of 6GB RAM and a runtime around 1min.
##
## -- Sven, 2017-03-29 (started 2017-03-22)

import sys, os
from aux import *

from postcactus.simdir import SimDir
from postcactus.grid_data import RegGeom, bounding_box
from postcactus import cactus_grid_h5, grid_data
import vtk # this takes 2sec
from vtk.util import numpy_support
from vtk.util import numpy_support

sd = SimDir("/home/sven/numrel/models3d/cosimas-nsbh/rhoh5-Jan-27-10-01/")

# sd.grid.xyz: OmniGridReader
print "All fields in the SimDir: ", sd.grid.xyz.all_fields()

it = sd.grid.xyz.get_iters("rho")
#print "All iterations in the rho 3D field: ", it

# cp: CompData
cp = sd.grid.xyz.read("rho", it[0])

### begin of option list

# Interpolation to the finest grid. This is not what we do.
interpolate_all_to_finest = False

# for debugging, only extract a subset of components:
# detect the components wich contain a common point, ie. the origin:
# [(k, i.reflevel(), i.contains([0,0,0])) for k,i in enumerate(origcp) if i.contains([0,0,0])]
Testing = False

# Combine unique points in the grid. You want this.
dounique = True

# Remove ghost zones. You want this.
stripGhosts = True

# Apply reflection symmetry. Still experimental.
# Actually, setting this to True does not do anything.
reflectSym = False

# Merge the components on each level before doing anything else. Thus we get a very
# simple CompData object which is fast to iterate on.
mergeLevelsBefore = True

# parallelism: Exploit huge numbers of component/reflevels.
doParallel = True

# binary VTK file output, else ASCII files. ASCII VTK files are much larger.
binaryVtkOutput = True

# name of the single output vtk file
outputVtkFilename = "full.vtk"

### end of option list

# This is the naive approach: Interpolate everything to the finest reflevel.
# Basically works, but takes very much storage in both RAM and disk and is
# incredibly slow.
if interpolate_all_to_finest:
	# come up with a RegGeom of the finest grid. This is inspired by
	# how grid_data.merge_geom(...) works.
	dx = cp.dx_finest()
	x0, x1 = bounding_box(cp)
	shape = ((x1-x0)/dx + 1.5).astype(np.int64) # why +1.5 !?
	finest_rg = RegGeom(shape, x0, dx)

	# finest_rg is now a geometry over the whole simulation data suitable
	# for interpolating everything on the finest grid.
	
	print "Interpolation on finest grid needs memory: ",sizeof_fmt(finest.size)
	cp.sample(finest_rg)

	# with my data, I need theoretically 20GB RAM for the cp.data.size and in reality
	# around 120GB RAM for the regular grid object.
	# The evaluation of cp.sample takes hours even on a brand new and large machine.
	# In principle, we could go from here and create a vtkRegularGrid with a file size
	# at the same order of magnitude. There is no sense to continue here.
	

# the vtkUnstructedGrid points for each voxel:
voxelNormBlock = np.array([
	# the 8 positions defining a voxel
	0, 0, 0, # origin
	1, 0, 0, # x=1
	0, 1, 0, # y=1
	1, 1, 0, # x=y=1
	0, 0, 1, # z=1
	1, 0, 1, # x=z=1
	0, 1, 1, # y=z=1
	1, 1, 1  # x=y=z=1
]).reshape(8,3)

def extractComponent((i, cpi)):#, cp):
	"""
	Maps a grid_data.RegData object to a tuple (field, points, rev) with each numpy arrays
	representing the same information as RegData in a suitable way for feeding into a VTK
	unstructure mesh.
	
	The RegData holds already the field data (which is just flattened/raveled). For the points,
	we build them up as a list from cpi.{dx,x0,x1}() and then build the difference to all other
	refinement levels in the global grid_data.CompData instance stored at `cp`.
	
	Basically this method thus generates a lot of meta data (coordinates) but reduces them
	afterwards heavily in order to produce the minimal possible vtkUnstructedGrid in the end.
	There is also the unique() call in order to reduce the number of total points, thus we
	output `rev` which allows to composite again the exact point list.
	
	Uses global variable cp.
	Uses global variable voxelNormBlock.
	This should probably be a method in CompData.
	"""
	print "Extracting Component %3i / %i ..." % (i, len(cp))
	#print "Reflevel:", cpi.reflevel(), "Extends:", cpi.x0(), cpi.x1()
	## field and centers always have similar dimensions. They are flattened
	## and field[i] corresponds to the three coordinates centers[:,i].
	field = cpi.data.ravel()
	dx = cpi.dx() # size of a cell in each direction
	dx2 = dx / 2.0 # half the cell size = length from center to cell boundary
	cells = cpi.coords_np1d() # cell positions of cpi. Somehow this is the lower left boundary or so.
	cells0 = cells  # lower left and upper right boundaries of 
	cells1 = cells + dx[:,n] # each cell

	# inspect all components at all reflevels and cut out appropriately
	# This allows disconnected finer refinement leveles, for instance two
	# still seperated neutron stars
	# reversed: Exploit fact that grid_data.CompData sorts all datasets basically by reflevel,
	# ie. reversed(cp) gives you the coarsest level first
	for j, rdi in enumerate(cp):#reversed(cp)):
		#if rdi.reflevel() >= cpi.reflevel() and not same_numpy_objs(rdi.data, cpi.data):
		if rdi.reflevel() > cpi.reflevel(): # If they are better refined then we
			# we compare against `their` (rdi) bbox with x0=lower left, x1=upper right
			x0, x1 = rdi.x0(), rdi.x1() # bounding box of rdi, cell positions or so.
			#dxr2 = rdi.dx() / 2.0 # half the cell size of rdi
			#bbox0 = x0 - dxr2  # their real bounding box, no more the cell centers
			#bbox1 = x1 + dxr2
			bbox0 = x0
			bbox1 = x1 + rdi.dx()
			
			overlapping_cells = np.all(( bbox0[:,n] < cells1 )&( cells0 < bbox1[:,n]), axis=0)
			# cut out coordinates inside the boundary box
			keep = np.invert(overlapping_cells)
			#keep = overlapping_cells
			#assert np.any(keep), "Cutting out everything due to Reflevel overlapping or so"
			if not np.any(keep) and len(field)>0:
				print "Comp %d<->%d Reflevel %d overlapping %d: Discarding all %d points" % (j, i, rdi.reflevel(), cpi.reflevel(), len(field))
				print "         inside bounding box ", x0, x1 
				
				# we can return here.
				empty = np.array([], dtype='float')
				return (empty, empty, empty)
			if not np.all(keep):
				# debugging output etc.
				print "Partially discarding %.1f%% points: " % (100*float(len(field[-keep]))/len(field))
				print "cp[i=%d] = "%i, cpi
				print "rd[j=%d] = "%j, rdi
				print "Their new bbox: bbox0=%s bbox1=%s" % (str(bbox0), str(bbox1))
				#print "Component their=%d vs my=%d Reflevel %d > %d:" % (j, i, rdi.reflevel(), cpi.reflevel())
				#print "  Discarding %d/%d points inside bounding box x0=%s, x1=%s"  % (len(field[-keep]), len(field), str(x0), str(x1))
				cells = cells[:,keep]
				field = field[keep]
				# recompute derived quantities
				cells0 = cells  # lower left and upper right boundaries of 
				cells1 = cells + dx[:,n] # each cell

	# Actually there is an approach to replace this loop by a few fast high level numpy operations,
	# using roughly
	#  X0,C = np.meshgrid( cp.each_x0, cpi.coords_np1d(), sparse=True )
	#  X1,C = np.meshgrid( cp.each_x1, cpi.coords_np1d(), sparse=True )
	#  np.all( (C < X0) & (C < X1), axis = 0)
	# but then it get's complicated.

	numcells = len(field)

	# set up the 8 points defining the voxel around each cell coordinate
	voxelBlock = voxelNormBlock * dx[n]
	SetupBoxCoordinates = lambda p: (cells + (p*dx)[:,n]).T
	
	gridpoints = np.rollaxis( np.array(map(SetupBoxCoordinates, list(voxelNormBlock))), 1)
	#gridpoints = np.repeat(cells0,8, axis=0) * voxelBlock.flatten()[:,n]
	numcoordinates = gridpoints.size
	numpoints = numcoordinates/3
	allpoints = gridpoints.reshape( (numpoints, 3) )
	assert numpoints/8 == numcells
	
	if dounique:
		#print "Finding unique grid points..."
		# store only distinct points, thus creating by a connected mesh along the way.
		# This operation is really slow.
		idx, rev = uniquerows(allpoints)
		assert np.all(allpoints == allpoints[idx][rev]) # like np.unique
		uniquepoints = allpoints[idx]
		allpoints = uniquepoints # switch to allpoints
	#assert np.all(allpoints[cellids] == gridpoints) # cellids can map back to the grid structure

	return (field, allpoints, rev)

print "Extract all components..."

if Testing:
	origcp = cp
	cp = [ cp[10], cp[15], cp[34], cp[58] ]

if stripGhosts:
	ghostycp = cp
	# You can play with the numbers of ghost removed in the lower left and upper right
	# edge of each refinement level.
	# Ensure to remove the ghosts *before* merging all components on a single reflevel because
	# merging removes the cell tagging as ghost boundaries.
	
	# These parameters here are fine tuned for the given h5 input file.
	
	default = None
	cp = cp.strip_ghosts(
		N0={ default: [3,3,3], 4: [3,2,3] },
		N1={ default: [2,2,2], 4: [2,3,2] }
	)


if mergeLevelsBefore:
	unmergedcp = cp
	cp = cp.get_merged_simple_all()

if reflectSym:
	for r in cp:
		# reflect for z.
		r.reflect(2)

# compose all boundaries as a single numpy structure, for high level operations
# (not yet used, see comments above)
cp.each_x0 = np.rollaxis(np.array([ rdi.x0()  for rdi in cp]), 1)
cp.each_x1 = np.rollaxis(np.array([ rdi.x1()  for rdi in cp]), 1)

# this snippet has to be in the main file, *AFTER* the definition of the final cp
# and extractComponent, otherwise the parallel section uses wrong variables.
from multiprocessing import Pool
pool = Pool()
if doParallel:
	mapfun = pool.map
else:
	mapfun = map

# pass the latest cp to the function, thus avoiding global variables...
#localExtractComponent = lambda i: extractComponent(i, cp)
res = mapfun(extractComponent, enumerate(cp))

# rev is still local, add the offsets for positions
globalpoints = 0
for local in res:
	field, allpoints, rev = local
	rev += globalpoints
	localpoints = len(allpoints)
	globalpoints += localpoints

def ext(num, desc):
	"Extract with message from the map(cp) result"
	ret = np.concatenate([r[num] for r in res if len(r[0])]) # impose filter for empty fields
	# "{;,}".format() is Python 2.7
	print desc % ( "{:,}".format(len(ret)), sizeof_fmt(ret.nbytes) )
	return ret

field = ext(0, "Holding %s scalar cell data (memory size: %s)")
allpoints = ext(1, "Holding %s points (memory size: %s)")
rev = ext(2, "Holding %s reverse links, uniquly reduced (memory size: %s)")

numcells = len(field)
numpoints = numcells*8

# This step now still takes some time. We could optimize it by setting up a suitable
# cellids and cellstrides already before.

# associated point Indices for cells:
if dounique:
	# using the distinct points, the cell mapping really contains information:
	cellids = rev.reshape(numcells,8) #np.array(np.split(rev, numcells))
else:
	cellids = np.array(np.split(np.arange(numpoints),numcells)) # untested so far
# faster code, with iterators
#if not dounique: rev = xrange(numpoints)
#assert len(rev)/8 == numcells

# insert the 8 for the cell stride format
the8 = np.zeros((numcells,1)) + 8.0
cellstrides = np.hstack((the8,cellids)).ravel().astype(np.int64)

print "Setting up VTK structure..."

vtkdtype = numpy_support.get_vtk_array_type(allpoints.dtype)
allvtkpoints = numpy_support.numpy_to_vtk(allpoints) # the deep=True is probably expensive
fieldvtk = numpy_support.numpy_to_vtk(field) #, deep=True)
#if not dounique: rev = np.array(list(rev)) # to be improved
#allcellsvtk = numpy_support.numpy_to_vtk(rev, array_type=vtk.VTK_ID_TYPE)
cellstridesvtk = numpy_support.numpy_to_vtk(cellstrides, array_type=vtk.VTK_ID_TYPE)

voxelPoints = vtk.vtkPoints()
voxelPoints.SetData(allvtkpoints) # an <numcells> x 3 arrays

#print voxelPoints

grid = vtk.vtkUnstructuredGrid()
grid.Allocate(8*numcells+1,1000)

print "Setting up %d cells in UnstructuredMesh ..." % numcells

# this is not really fast:
#def makeVoxel(cell):
#	idList = vtk.vtkIdList()
#	assert len(cell) == 8
#	map(idList.InsertNextId, cell)
#	return idList
#
## this is not really fast
#voxels = mapfun(makeVoxel, window(rev, 8))
#
#print "Inserting..."
# do insertion serially, for the moment
#for idList in voxels:
#	grid.InsertNextCell(vtk.VTK_VOXEL, idList)

# not that slow code
#for cell in window(rev, 8):
doCellLoop=False
if doCellLoop:
	for cell in cellids:
		# setup the voxel connection
		idList = vtk.vtkIdList()
		assert len(cell) == 8
		map(idList.InsertNextId, cell)
		grid.InsertNextCell(vtk.VTK_VOXEL, idList)

if not doCellLoop:
	# inserting all at once. This is very fast
	ca = vtk.vtkCellArray()
	ca.SetCells(numcells, cellstridesvtk)
	grid.SetCells(vtk.VTK_VOXEL, ca)

grid.SetPoints(voxelPoints)
grid.GetCellData().SetScalars(fieldvtk)
#grid.GetCellData().SetName("Field") # does not work

#print grid

print "Writing VTK file ", outputVtkFilename, " as %s VTK file ..." % ("binary" if binaryVtkOutput else "ascii")

writer = vtk.vtkUnstructuredGridWriter()
writer.SetFileName(outputVtkFilename)
if binaryVtkOutput:
	writer.SetFileTypeToBinary()
else:
	writer.SetFileTypeToASCII()
writer.SetInputData(grid)
success = (writer.Write() == 1)

print "Writing done" if success else "Error at writing!"

