# Offloading helpers, monkey patched PostCactus classes, etc.

import sys,os
import numpy as np
from itertools import islice


sys.path.append("../pycactuset/PostCactus") # change to where you installed PostCactus
from postcactus import grid_data

n = np.newaxis # shortcut

def sizeof_fmt(num, suffix='B', fac=1024.0):
    "By default, actually printing KiB, MiB, GiB, but people get confused. Set suffix='iB' to turn this on."
    for unit in ['']+list("KMGTPEZ"):
        if abs(num) < fac:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= fac
    return "%.1f%s%s" % (num, 'Y', suffix)

def uniquerows(a):
	"Find unique rows in a (2,...) shape ndarray."
	b = np.ascontiguousarray(a).view(np.dtype((np.void, a.dtype.itemsize * a.shape[1])))
	garbage, idx, rev = np.unique(b, return_index=True, return_inverse=True)
	return (idx, rev)

def window(seq, n=2):
    "Returns a sliding window (of width n) over data from the iterable"
    "   s -> (s0,s1,...s[n-1]), (s1,s2,...,sn), ...                   "
    it = iter(seq)
    result = tuple(islice(it, n))
    if len(result) == n:
        yield result    
    for elem in it:
        result = result[1:] + (elem,)
        yield result

def same_numpy_objs(a,b):
	"Check if two objects hold same stuff"
	if (a.shape == b.shape):
		return np.all(a == b)
	else:
		return False

###
### Monkeypatching the postcactus.grid_data module
###

def regdata_strip_ghosts(self, N0=None, N1=None):
	"""
	Returns a new instance of RegData which holds cropped data without
	the ghost zones, in a consistent manner with the package.
	
	If parameter `N` is given, only up to N ghosts are removed.
	By default, all ghost zones are removed. `N` can be a list with
	three entries (one entry per dimension) or just a plain number.
	"""
	assert np.sum(self.dim_ext()) == 3, "Stripping ghost zones is currently only implemented for 3 dimensions  but trivial to extend for lower."
	assert np.all(self.nghost()>0), "This RegData instance has no ghost zones to strip"
	# by default, remove all ghost zones
	if N0 is None: N0 = self.nghost() # N0 = removed ghost zones lower left
	if N1 is None: N1 = self.nghost() # N0 = removed ghost zones lower left
	
	if isinstance(N0, list): N0 = np.array(N0)
	if isinstance(N1, list): N1 = np.array(N1)
	if not isinstance(N0, np.ndarray): N0 = np.array([N0, N0, N0]) # assume dimension 3.
	if not isinstance(N1, np.ndarray): N1 = np.array([N1, N1, N1]) # assume dimension 3.
	
	# remaining ghost zones: If N0 != N1, cast the remainding points no more
	# as ghost points.
	remaining = np.min([self.nghost() - N0, self.nghost() - N1], axis=0)
	
	assert np.all(remaining >= 0), "There are only '%s' ghost zones, removing '%s' requested" % (str(self.nghost()), str(N))
	#assert np.all(N>0), "Please strip at least something in each direction" # not sure

	# cf. visualize.plot_grid_struct.
	# We work with cell centers everywhere here, consistently to PostCactus.
	current_dg = self.dx()*self.nghost()
	nx0 = self.x0() + self.dx()*N0
	nx1 = self.x1() - self.dx()*N1
	
	# the slice allows to truncate the data equally at all dimensions
	slice3d = [slice(rem0,-rem1) for (rem0, rem1) in zip(N0,N1)]
	newdata = self.data[slice3d]
	
	return grid_data.RegData(nx0, self.dx(), newdata,
		outside_value=self.outside_value, 
		reflevel=self.reflevel(), component=self.component(), 
		nghost=remaining if np.all(remaining>=0) else None,
		time=self.time, iteration=self.iteration)

def regdata_coords_np(self):
	"""
	Gives a single numpy array of what coords2d gives. This method is more than two
	times faster than creating a np.array(self.coords2d()).
	It gives the same result:
	
	> self = someRegDataInstance
	> np.all(self.coords_np() == np.array(self.coords2d()))
	True
	
	Note that these coordinate arrays are always with cell centered positions, ie. they
	indicate the center of each cell.
	
	Note it's a mystery if the coordinates are actually centers or not.
	"""
	i = np.indices(self.shape())
	return (i.T*self.dx()+self.x0()).T

def regdata_coords_np1d(self):
	"""
	Gives three list for each coordinates in a 1d fashion.
	Also here it is much faster then composing an array by hand:
	
	> np.all(self.coords_np1d() == np.array([a.ravel() for a in self.coords2d()]))
	True
	
	"""
	# assumes 3d
	coords = self.coords_np()
	return coords.reshape(3, np.prod(coords.shape[1:]))

def compdata_strip_ghosts(self, N0=None, N1=None):
	"""
	Returns a new instance of CompData where no more ghost zones are present.
	
	The parameters N0 and N1 indicate the number of ghost zones to be removed in the
	lower left (i.e according to x0) or upper right (according to x1).
	
	If the individual parameter is a number, all ghost levels are equally stripped in
	each dimension.
	
	If the individual parameter is a list, all ghost levels are stripped according to the
	entries, ie. [1,2,1] will remove 1 ghost zone in x direction, 2 in y direction and 
	1 in z direction. This is similar to the shape of nghosts().
	
	If the individual parameter is None, all available ghost levels are stripped. If you
	want no ghost level to be stripped, give 0 or in a list like [1,0,2] to have no level
	stripped in y direction.
	
	If the individual parameter is a dictionary, it will interpreted as mapping from the
	reflevel to the number of ghost zones to be removed (which can be again a single number
	or a dictionary). For instance, N0={ 1: [1,2,1], 2: 2, 3: None } will strip fancy at
	level 1, two zones at level 2 and all zones at level 3. If you provide a key "None",
	it will be used for all reflevels not specified in the dict, for instance
	N1={None: 2, 1: None } will remove all ghost zones on level 1 and 2 ghost zones on
	all other levels.
	"""
	def normalize2dict(Ni):
		return { l: Ni for l in self.get_levels() } if not isinstance(Ni, dict) else Ni
	def fillUpMissing(Ni):
		for l in self.get_levels():
			if not l in Ni:
				if None in Ni:
					Ni[l] = Ni[None]
				else:
					raise ValueError("Missing value for reflevel %d in dict %s. Probably you want to provide a default key 'None'" % (l, str(Ni)))
		return Ni
	N0 = fillUpMissing(normalize2dict(N0))
	N1 = fillUpMissing(normalize2dict(N1))
	
	newdat = [grid_data.RegData.strip_ghosts(this, N0[this.reflevel()], N1[this.reflevel()]) for this in self]
	return grid_data.CompData(newdat, self.outside_value)

def compdata_get_merged_simple_all(self):
	"""
	Returns a new instance of CompData with all refinement levels each merged together
	using get_merged_simple.
	"""
	levels = [self.get_merged_simple(l) for l in self.get_levels() ]
	return grid_data.CompData(levels, self.outside_value)


# monkeypatching: This is changing directly the imported modules, so you can import
# them later on the usual way and can make use of the patched versions

grid_data.RegData.coords_np = regdata_coords_np
grid_data.RegData.coords_np1d = regdata_coords_np1d
grid_data.RegData.strip_ghosts = regdata_strip_ghosts

grid_data.CompData.strip_ghosts = compdata_strip_ghosts
grid_data.CompData.get_merged_simple_all = compdata_get_merged_simple_all

