Creating STL files for 3D printing from Cactus/Carpet simulations
=================================================================

This repository contains python scripts to create 3D prints from simulations
made with the [Einstein Toolkit](https://einsteintoolkit.org/), for instance
in order to 3D print the density field of a timestep in a binary neutron star
simulation.

Carpet meshes as unstructured grid
----------------------------------

The popular CarpetHDF5 reader for importing the 3D files created by
[Carpet](https://carpetcode.org/) into Visit (Source for instance
at [Visit SVN](http://visit.ilight.com/svn/visit/trunk/src/databases/CarpetHDF5/)
or [Cactus SVN](https://svn.cactuscode.org/VizTools/CarpetHDF5/trunk/),
Documentation at [Cactuscode website](http://cactuscode.org/documentation/visualization/VisIt/))
fails when exporting the Visit database to other formats such as the
[STL file format](https://en.wikipedia.org/wiki/STL_(file_format)).

This is as multiple refinement levels are exported ontop of each other. In order
to circumvent this shortcoming, this repository uses the
[PyCactusET/PostCactus](https://bitbucket.org/DrWhat/pycactuset) reader for
Carpet HDF5 files to compose a VTK unstructured mesh. These VTK files are pretty
large as they contain explicitely the meatadata for any grid point but they ensure
that there is *no* overlap between the refinement levels. They can be opened with
any standard VTK viewer such as the free [Visit](https://wci.llnl.gov/codes/visit/)
and [ParaView](http://www.paraview.org/) visualization programs.

### Some details about the grid in CarpetHDF5

* The CarpetHDF5 reader removes all ghost zones.
* The reader sets up a vtkStructuredGrid when in Multipatch mode and a
  vtkRectilinearGrid when in unigrid/single reflevel mode.
* The CarpetHDF5 reader does not really distinguish between cell centered and
  point/grid node centered data. 

### Some details about the grid in PostCactus

In PostCactus, the cell positions always indicate the lower left of a cell. This also
applies for the boundary boxes defined by the lower left and upper right coordinate,
(x0,x1) with x0, x1 \in R^3. Thus, when computing the real boundary box for a regular
grid, one has to take into account (x0,x1+dx) with dx \in R^3.

### Free Ressources about VTK

* The amazing [Visit users wiki](http://visitusers.org/index.php?title=Main_Page)
* The [VTK file formats](www.vtk.org/VTK/img/file-formats.pdf) 20 pages fact sheet
* Of course the [VTK API](http://www.vtk.org/doc/nightly/html/) (very heavy)
* There are [VTK textbooks](https://www.kitware.com/what-we-offer/#books).
  And eBook/PDF versions in the web.


Export from VTK to STL
----------------------

The postprocessing of a VTK file in terms of a contour plot to a a STL file is pretty
straightforward. These STL files are then subject to editing with programs such as
[Blender](https://www.blender.org/) in order to implement symmetries, cuts, etc.

This will be added to the repository as soon as implemented. We want to supply
python scripts for Visit and Blender.



